<!DOCTYPE html>
<html>
	<head>
		<title><?php bloginfo('name'); ?></title>
		
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bar/bar.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/dark/dark.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/default/default.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/light/light.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/nivo-slider.css"/>
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>"/>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/fonts/stylesheet.css"/>
		<?php wp_head(); ?>
	</head>
	<body>
		<div class="fix main">
			<div class="fix header">
				<a href="<?php bloginfo('home'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.jpg" alt=""/></a>
			</div>