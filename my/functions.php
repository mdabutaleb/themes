<?php 


function rasel_widget_areas() {
	register_sidebar( array(
		'name' => __( 'Left Menu', 'rasel' ),
		'id' => 'left_sidebar',
		'before_widget' => '<div class="single_sidebar">',
		'after_widget' => '</div>',
	    'before_title' => '<h2>',
	    'after_title' => '</h2>',
	) );
}
add_action('widgets_init', 'rasel_widget_areas');



?>