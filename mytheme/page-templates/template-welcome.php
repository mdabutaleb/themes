<?php
/*
 * Template Name : Welcome Template
 * @package WordPress
 * @subpackage sumon
 * @since sumon 1.0
 */
get_header();
?>
<?php get_template_part('block'); ?>
<div class="main_wrap">
    <!--{{{{{{Start Content Area  }}}}}}}-->
    <div class="content">
        <?php if (have_posts()): ?><?php while (have_posts()) : the_post(); ?>
                <h3><?php the_title ?></h3>
                <?php the_content(); ?>
            <?php endwhile; ?>
        <?php else : ?> 
            <h3><?php _e('404 Error#58; Not Found', 'bilanti'); ?></h3>
        <?php endif; ?>
    </div>
</div><!--[[[[[[   End Client Part    ]]]]]]-->
<?php
get_template_part('bigfooter');
get_template_part('smallfooter');
wp_footer();
?>