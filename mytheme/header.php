<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes('xhtml');?> >
    <head>
         <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
         <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>"; charset="<?php bloginfo('charset'); ?>"/>
         <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" media="screen"/>
         <link type="text/css" href="<?php echo get_template_directory_uri();?>/css/fonts.css" rel="stylesheet">
         <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>image/favicon.ico" type="image/x-icon">
             <!--pingback-->
        <link rel="pingback" href="<?php bloginfo('pingback_url') ?>"/>
        <!--Archive-->
        <?php wp_get_archives('type=monthly&format=link&limit=12') ?>
        <?php //comments_popup_script(); // off by default ?>
        <?php wp_head(); ?>
           

    </head>
    <body>
        <div class="full_wrap">
            <div class="main_header">
                <div class="main_wrap">

                    <div class="header">
                        <div class="logo">
                            <img src="<?php echo get_template_directory_uri();?>/image/logo1.png" >
                                <!--Display site Title & Descriptoin-->
<!--                                <h1><?php echo bloginfo('name'); ?></h1>
                                <h2><?php echo bloginfo('description'); ?></h2>-->
                        </div>
                        <div class="menu">
                            <?php wp_nav_menu(array('theme_location'=> 'wpj-main-menu'))?>
<!--                            <ul>
                                <li><a href="index.php"> Home </a></li>
                                <li><a href="about.php"> About </a></li>
                                <li><a href="docs/Md. Abutaleb.pdf" target="_blank"> Resume </a></li>
                                <li><a href="portfolio.php"> Portfolio </a></li>
                                <li><a href="http://www.abutaleb.com" target="_blank">Blog </a></li>
                                <li><a href="contact.php">Contact </a></li>
                                <li><a href="user.php">Register </a></li>
                                <li><a href="user.php">Login</a></li>
                            </ul> -->
                        </div><!--End menu-->
                        <div class="smm_logo">

                            <a href="http://www.facebook.com/sumonmhd.cse" target="blank" ><div class="f">f</div></a>            
                            <a href="http://www.twitter.com/howtopten" target="blank" ><div class="t">t</div></a> 
                            <a href="http://www.pinterest.com/howtopten" target="blank" ><div class="p">p</div></a>
                            <a href="https://www.youtube.com/channel/UCAOZ6MSJ2gqnothXe4tx1Jw" target="blank" ><div class="y">y</div></a>
                            <a href="https://plus.google.com/u/0/+SumonMahmud-howtopten/posts" target="blank" ><div class="g">g+</div></a> 
                            <a href="http://www.linkedin.com/sumonmhd.cse" target="blank" ><div class="l">in</div></a>
                            <div class="clr"></div>    
                        </div>

                    </div>  <!--End of header--> 

                </div><!--End main wrap for header-->

            </div>