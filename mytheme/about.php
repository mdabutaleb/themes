<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>About | Md. Abutaleb</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link type="text/css" href="css/style.css" rel="stylesheet">
        <link type="text/css" href="css/fonts.css" rel="stylesheet">
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="image/favicon.ico" type="image/x-icon">
    </head>
    <body>
        <div class="full_wrap">
            <div class="main_header">
                <div class="main_wrap">

                    <div class="header">
                        <div class="logo">
                            <img src="image/logo1.png">
                        </div>
                        <div class="menu">
                            <ul>
                                <li><a href="index.php"> Home </a></li>
                                <li><a href="about.php"> About </a></li>
                                <li><a href="docs/Md. Abutaleb.pdf" target="_blank"> Resume </a></li>
                                <li><a href="portfolio.php"> Portfolio </a></li>
                                <li><a href="http://www.abutaleb.com" target="_blank">Blog </a></li>
                                <li><a href="contact.php">Contact </a></li>
                                <li><a href="user.php">Register </a></li>
                                <li><a href="user.php">Login</a></li>
                            </ul> 
                        </div><!--End menu-->
                        <div class="smm_logo">

                            <a href="http://www.facebook.com/sumonmhd.cse" target="blank" ><div class="f">f</div></a>            
                            <a href="http://www.twitter.com/howtopten" target="blank" ><div class="t">t</div></a> 
                            <a href="http://www.pinterest.com/howtopten" target="blank" ><div class="p">p</div></a>
                            <a href="https://www.youtube.com/channel/UCAOZ6MSJ2gqnothXe4tx1Jw" target="blank" ><div class="y">y</div></a>
                            <a href="https://plus.google.com/u/0/+SumonMahmud-howtopten/posts" target="blank" ><div class="g">g+</div></a> 
                            <a href="http://www.linkedin.com/sumonmhd.cse" target="blank" ><div class="l">in</div></a>
                            <div class="clr"></div>    
                        </div>

                    </div>  <!--End of header--> 

                </div><!--End main wrap for header-->

            </div> <!--End main header-->
            <div class="headerbottomblock"><!--Header blog--></div>
            <div class="main_wrap">
                <div class="content"><!-- Start About -->
                    <div class="about1">
                        <div class="about_left">
                            <h3>Who am i?</h3><br>
                            <p align="justify">
                                Thanks for visit my personal website. 
                                I am Md. Abutaleb. My nick name is Sumon Mahmud. 
                                I am studying B.Sc in Computer Science & 
                                Engineering at (NUB) Northern University 
                                Bangladesh. My campus is near of Banani, Dhaka. 
                                My last semster going on. ! ! <br><br>
                                I am not only studying  but 
                                also working on a freelancing markeplace as a 
                                freelance web design. But i'm looking for a full
                                time job. 
                            </p>

                        </div>
                        <div class="about_right">
                            <h3>My Service</h3><br>
                            <p align="justify">Basically i am providing Web design services. 
                                Always i am trying to provide high Quality and 
                                best Web Design Service. Because i belive that quality 
                                is better than quantity. So hire me for quality works.</p><br>
                            <b>My services are following: </b><br>
                            <ul>
                                <li>PSD / Jpeg to HTML, HTML5, CSS3</li>
                                <li>Existing Website to better website</li>
                                <li>PSD to Joomla & Wordpress</li>


                            </ul>
                        </div>
                    </div>

                </div><!-- End About -->
            </div>


        </div> <!--End full wrap for main header-->
        <div class="main_portfolio">
            <div class="main_wrap">
            </div>
            <div class="clr"></div>
        </div> 

        <div class="full_wrap"> 
            <div class="main_wrap">


            </div>
        </div>
        <div class="headerbottomblock"></div>
        <div class="full_big_footer">
            <div class="bigfooter">

                <div class="bfooter1">
                    <h3> Useful Links</h3>
                    <ul>
                        <li><a href="https://www.odesk.com/users/Web-Developer-and-Proper-SEO-Service-Provider_~0152bc340abea82e1f?tot=2&pos=0" target="_blank"># My oDesk profile</a><br></li>
                        <li><a href="index.html#"># My Elance Profile</a></li>
                        <li><a href="http://howtopten.com/category/tutorial/seo-search-engine-optimization" target="_blank"/># SEO Tutorial</a></li>
                        <li><a href="http://howtopten.com/category/tutorial/web-design/" target="_blank"># Web Design Tutorial</a></li>
                        <li><a href="http://howtopten.com/category/smm-services/" target="_blank"># Social Media Tutorial</a></li>
                        <li><a href="http://howtopten.com/category/seo-services-2" target="_blank"/># SEO Service Provider</a></li>
                        <li><a href="http://howtopten.com/about/our-team/" target="_blank"/># Meet With My Team</a></li>
                    </ul>
                </div>
                <div class="bfooter2">
                    <h3> Experience</h3>
                    <ul>
                        <li><mark>HTML</mark>&nbsp;&nbsp;&nbsp;: 9 Out of 10</li>
                        <li><mark>CSS</mark>&nbsp;&nbsp;&nbsp;: 9 Out of 10</li>
                        <li><mark>Java Script</mark>&nbsp;&nbsp;&nbsp;: 9 Out of 10</li>
                        <li><mark>Jquery</mark>&nbsp;&nbsp;&nbsp;: 4 Out of 10</li>
                        <li><mark>PHP</mark>&nbsp;&nbsp;&nbsp;: 4 Out of 10</li>
                        <li><mark>Joomla</mark>&nbsp;&nbsp;&nbsp;: 7 Out of 10</li>
                        <li><mark>WordPress</mark>&nbsp;&nbsp;&nbsp;: 8 Out of 10</li>

                    </ul>
                </div>
                <div class="bfooter3">
                    <h3> Custom Menu</h3>
                    <ul>
                        <li><a href="index.php">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;~~ &nbsp;&nbsp;Home &nbsp;&nbsp;~~</a></li>
                        <li><a href="about.php">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;~~ &nbsp;&nbsp;About&nbsp;&nbsp; ~~</a></li>
                        <li><a href="resume.php">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;~~ &nbsp;&nbsp;Resume&nbsp;&nbsp; ~~</a></li>
                        <li><a href="portfolio.php">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;~~ &nbsp;Portfolio&nbsp; ~~</a></li>
                        <li><a href="blog.php">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;~~ &nbsp;&nbsp;Blog&nbsp;&nbsp; ~~</a></li>
                        <li><a href="contact.php">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;~~ &nbsp;&nbsp;Contact&nbsp;&nbsp; ~~</a></li>
                        <li><a href="user.php">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;~~ &nbsp;&nbsp;User&nbsp;&nbsp; ~~</a></li>
                    </ul> 
                </div>
                <div class="bfooter4">
                    <h3> Quick Message</h3>
                    <form action="" method="get" style="background-color: gainsboro">
                        <table border="0" align="center" width="80%" bgcolor="" >
                            <tr>
                                <td> 
                                    <input  type="text" name="full_name" placeholder="Enter Full Name" >
                                </td>
                            </tr>
                            <tr>
                                <td> 
                                    <input  type="email" name="email_address" placeholder="Ex: mail@mail.com" >
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <textarea name="message" placeholder="Enter Your Message" cols="30" rows="2" ></textarea>
                                </td> 
                            </tr>
                        </table>
                        <h2>Send</h2>
                    </form>

                </div>
                <div class="clr">

                </div>
            </div> <!--End of big footer-->
        </div> <!--End of full big footer-->
        <div class="smallfooter">
            <div class="mainsmallfooter">
                <div class="f1">
                    <p> Md Abutaleb &COPY 2014</p>
                </div>
                <div class="f2">
                    <p>Cell : +880 1717-613327 | Skype : cse.sumon | info@howtopten.com </p>
                </div>
                <div class="f1">
                    <a href="http://www.facebook.com/sumonmhd.cse" target="blank" ><div class="f">f</div></a>            
                    <a href="http://www.twitter.com/howtopten" target="blank" ><div class="t">t</div></a> 
                    <a href="http://www.pinterest.com/howtopten" target="blank" ><div class="p">p</div></a>
                    <a href="https://www.youtube.com/channel/UCAOZ6MSJ2gqnothXe4tx1Jw" target="blank" ><div class="y">Y</div></a>
                    <a href="https://plus.google.com/u/0/+SumonMahmud-howtopten/posts" target="blank" ><div class="g">g+</div></a> 
                    <a href="http://www.linkedin.com/sumonmhd.cse" target="blank" ><div class="l">in</div></a>
                    <div class="clr"></div>
                </div>

            </div>
        </div>

        <!--End full wrap-->
    </body>
</html>

<!--<div class="full_wrap"> 
    <div class="main_wrap">
    </div>
</div>-->
