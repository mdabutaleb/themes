<?php
/**
 * Sumon Mahmud Theme Defination
 * @package WordPress
 * @subpackage sumon mahmud
 */
 
function bilanti_scripts(){
    if( is_singular()) wp_enqueue_script('comment-reply');
}
add_action('wp_enqueue_scripts', 'bilanti_scripts');

//WordPress Menu Editor **** **********/ 
    // add menu support and fallback menu if menu doesn't exist 
    add_action('init', 'wpj_register_menu');
    function  wpj_register_menu(){
        if(function_exists('register_nav_menu')){
            register_nav_menu('wpj-main-menu', __('menu', 'bilanti'));
        }
    }
    register_nav_menu('menu_footer', __ ('Footer menu', 'bilanti')); 